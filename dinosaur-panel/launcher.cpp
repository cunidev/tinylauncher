#include <QProcess>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QVariantMap>
#include <QIcon>
#include <QListWidgetItem>
#include <QSize>


#include "launcher.h"
#include "ui_launcher.h"


// https://github.com/alamminsalo/qml-launcher/blob/master/src/main.cpp
struct AppInfo {
    QString name;
    QString icon = "application";
    QString exec;
    bool show = true;
};

QVariantList apps() {
    QVariantList apps;

    QDir dir("/usr/share/applications");
    foreach (auto fn, dir.entryList(QStringList() << "*.desktop", QDir::Files)) {
        QFile file(dir.filePath(fn));
        if (file.open(QIODevice::ReadOnly)) {
            QTextStream in(&file);

            AppInfo app;

            bool foundDesktopEntry = false;

            while (!in.atEnd()) {
                QString line = in.readLine();

                if (line.trimmed().isEmpty())
                    continue;

                if (!foundDesktopEntry) {
                    if (line.contains("[Desktop Entry]"))
                        foundDesktopEntry = true;
                    continue;
                }
                else if (line.startsWith('[') && line.endsWith(']')) {
                    break;
                }

                QStringList values = line.split("=");
                QString name = values.takeFirst();
                QString value = QString(values.join('='));

                if (name == "Name") {
                    app.name = value;
                }

                if (name == "NoDisplay" && value != "") {
                    app.show = false;
                }
                if (name == "OnlyShowIn" && value != "") { // todo: find value with a sense / don't hardcode to empty
                    app.show = false;
                }

                if (name == "Icon") {
                    app.icon = value;
                    QIcon icon = QIcon::fromTheme(app.icon);
                    if (icon.isNull()) {
                        // TODO: fix false positives!
                        qDebug()<< "null icon:" << app.icon;
                        app.icon = "application-x-executable";
                    }
                }

                if (name == "Exec") {
                    app.exec = value.remove("\"").remove(QRegExp(" %."));
                    // todo: don't add if exec does not exist
                }
            }

            if(app.show) {
                // sort
                QVariantList::iterator i;
                for(i = apps.begin(); i != apps.end(); i++) {
                    QString name = (*i).toStringList()[0];
                    if(QString::compare(name, app.name, Qt::CaseInsensitive) > 0) {
                        break;
                    }
                }

                apps.insert(i, QStringList() << app.name << app.icon << app.exec);
            }
        }
    }
    return apps;
}


Launcher::Launcher(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Launcher)
{
    ui->setupUi(this);
    ui->appList->setIconSize(QSize(48,48));
    ui->appList->setSpacing(8);
    ui->appNamePlaceholder->hide();
    ui->searchBtn->hide(); // not implemented :)

    populateAppList();
}

Launcher::~Launcher()
{
    delete ui;
}

void Launcher::_emptyAppList() {
    while(ui->appList->count()>0)
    {
      delete ui->appList->takeItem(0);
    }
}

void Launcher::refreshAppList() {
    this->populateAppList(true);
}

void Launcher::populateAppList(bool forceRefresh) {
    ui->refreshBtn->setIcon(QIcon::fromTheme("content-loading-symbolic"));
    ui->refreshBtn->repaint(); // needed to draw button before potential freeze

    _emptyAppList();
    ui->appsLabel->setText("All apps");

    if(app.length() == 0 || forceRefresh) {
        app = apps();
        _emptyAppList();
        for(QVariantList::iterator i = app.begin(); i != app.end(); i++) {
            QString name = (*i).toStringList()[0];
            QIcon icon = QIcon::fromTheme( (*i).toStringList()[1] );

            QListWidgetItem *q = new QListWidgetItem(icon, name);
            ui->appList->addItem(q);
        }
    }

    ui->refreshBtn->setIcon(QIcon::fromTheme("view-refresh-symbolic"));
}
void Launcher::populateOpenAppsList() {
    _emptyAppList();
    ui->appsLabel->setText("Open apps");

    QIcon icon = QIcon::fromTheme("application-x-addon");
    QListWidgetItem *q1 = new QListWidgetItem(icon, "Not implemented yet");
    ui->appList->addItem(q1);
}

void Launcher::appClicked() {
    ui->appList->hide();
    ui->appsLabel->hide();
    ui->appNamePlaceholder->show();
    ui->refreshBtn->hide();

    QString target = ui->appList->selectedItems()[0]->text();

    for(QVariantList::iterator i = app.begin(); i != app.end(); i++) {
        QString name = (*i).toStringList()[0];
        QString exec = (*i).toStringList()[2];

        if(name == target) {
            QProcess::startDetached(exec);
        }
    }
}
void Launcher::goHome() {
    ui->appList->show();
    ui->appsLabel->show();
    ui->appNamePlaceholder->hide();
    ui->refreshBtn->show();
    this->populateAppList();
}

void Launcher::showOpenApps() {ui->appList->show();
    ui->appsLabel->show();
    ui->refreshBtn->show();
    ui->appNamePlaceholder->hide();
    this->populateOpenAppsList();
}


