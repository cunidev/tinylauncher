#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QWidget>

namespace Ui {
class Launcher;
}

class Launcher : public QWidget
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = nullptr);
    ~Launcher();
    void populateAppList(bool forceRefresh = false);
    void populateOpenAppsList();
    void goHome();
    void showOpenApps();

private slots:
    void refreshAppList();
    void appClicked();
private:
    void _emptyAppList();
    Ui::Launcher *ui;
    QVariantList app;
};

#endif // LAUNCHER_H
