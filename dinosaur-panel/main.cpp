#include "panel-top.h"
#include "panel-bottom.h"
#include "launcher.h"
#include <QApplication>
#include <QRect>
#include <QScreen>

const int TOP_HEIGHT = 32;
const int BOTTOM_HEIGHT = 40;


void _setupPanel(QWidget *w, bool isBottom) {
    QRect screenRect = QApplication::primaryScreen()->geometry();

    w->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);
    w->setAttribute(Qt::WA_ShowWithoutActivating); // does this even work?
    w->setAttribute(Qt::WA_DeleteOnClose);
    w->setAttribute(Qt::WA_X11NetWmWindowTypeDock);

    if(isBottom) {
        w->resize(screenRect.width(), BOTTOM_HEIGHT);
        w->move(0, screenRect.height() - BOTTOM_HEIGHT);
        w->show();
    } else {
        w->resize(screenRect.width(), TOP_HEIGHT);
        w->move(0, 0);
        w->show();

    }


}
void _setupLauncher(QWidget *w) {
    w->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint);
    w->showMaximized();
    w->setAttribute(Qt::WA_ShowWithoutActivating);
    w->setAttribute(Qt::WA_DeleteOnClose);
    w->setAttribute(Qt::WA_X11NetWmWindowTypeDesktop); // what should be the right type for this???
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Launcher launcher;

    PanelTop panelTop(nullptr, &launcher);
    PanelBottom panelBottom(nullptr, &launcher);

    _setupPanel(&panelTop, false);
    _setupPanel(&panelBottom, true);
    _setupLauncher(&launcher);


    return a.exec();
}
