#include <QProcess>

#include "panel-bottom.h"
#include "ui_panel-bottom.h"

PanelBottom::PanelBottom(QWidget *parent, Launcher *launcher):
    QWidget(parent),
    ui(new Ui::PanelBottom)
{
    this->launcher = launcher;
    ui->setupUi(this);
}

PanelBottom::~PanelBottom()
{
    delete ui;
}

void PanelBottom::btnKeyboard() {
    qDebug("KEYBOARD");
    // lol
    QProcess::startDetached("notify-send \"No virtual keyboard installed!\"");
}
void PanelBottom::btnMultitasking() {
    qDebug("MULTITASKING");
    this->launcher->showOpenApps();

    QProcess::startDetached("wmctrl -k off"); //lol
}
void PanelBottom::btnHome() {
    qDebug("HOME");
    // TODO: minimize all
    QProcess::startDetached("wmctrl -k on");
    this->launcher->goHome();


}

