#include <QProcess>

#include "panel-top.h"
#include "ui_panel-top.h"

PanelTop::PanelTop(QWidget *parent, Launcher *launcher) :
    QWidget(parent),
    ui(new Ui::PanelTop)
{
    this->launcher = launcher;
    ui->setupUi(this);
}

PanelTop::~PanelTop()
{
    delete ui;
}

void PanelTop::btnClose() {
    qDebug("CLOSE");

    QProcess::startDetached("wmctrl -c :ACTIVE:"); // lol
    // btw, don't close if on home/launcher/no apps open/...

    // TODO: minimize all
    QProcess::startDetached("wmctrl -k on");
    this->launcher->goHome(); // makes sense?
}
