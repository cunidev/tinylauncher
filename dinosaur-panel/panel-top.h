#ifndef PANELTOP_H
#define PANELTOP_H

#include <QWidget>
#include "launcher.h"

namespace Ui {
class PanelTop;
}

class PanelTop : public QWidget
{
    Q_OBJECT

public:
    explicit PanelTop(QWidget *parent = nullptr, Launcher *launcher = nullptr);
    ~PanelTop();

private:
    Launcher *launcher;
    Ui::PanelTop *ui;

private slots:
    void btnClose();
};

#endif // PANELTOP_H
